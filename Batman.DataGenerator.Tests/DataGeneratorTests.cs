﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System.Collections.Generic;
using Batman.Interface.Model;

namespace Batman.DataGenerator.Tests
{
  [TestClass]
  public class DataGeneratorTests
  {
    [TestMethod]
    [DataRow(300, 400, 100)]
    [DataRow(200, 600, 40)]
    [DataRow(100, 700, 30)]
    [DataRow(500, 100, 20)]
    [DataRow(200, 400, 50)]
    public void GeneratesDataCorrectly(int networkCount, int nodesPerNetwork, int clientsPerNode)
    {
      var dataGenerator = new Generator.DataGenerator();

      var output = dataGenerator.CreateInitialData(networkCount, nodesPerNetwork, clientsPerNode).ToList();

      NetworksAssert(networkCount, nodesPerNetwork, clientsPerNode, output);
    }

    [TestMethod]
    [DataRow(600, 40)]
    [DataRow(700, 30)]
    [DataRow(100, 20)]
    public void GenerateRandomNetworkCorrectly(int nodesPerNetwork, int clientsPerNode)
    {
      var dataGenerator = new Generator.DataGenerator();

      var output = dataGenerator.CreateRandomNetwork(nodesPerNetwork, clientsPerNode);

      Assert.IsFalse(string.IsNullOrEmpty(output.Location));
      NodesAssert(nodesPerNetwork, clientsPerNode, output);
    }

    [TestMethod]
    public void GeneratesInfoForClientFromInitialDataSetCorrectly()
    {
      for (int networkCount = 1; networkCount < 30; networkCount += 10)
      {
        for (int nodesPerNetwork = 1; nodesPerNetwork < 100; nodesPerNetwork += 20)
        {
          for (int clientsPerNode = 1; clientsPerNode < 100; clientsPerNode += 10)
          {
            var dataGenerator = new Generator.DataGenerator();

            var initialData = dataGenerator.CreateInitialData(networkCount, nodesPerNetwork, clientsPerNode);
            var client = dataGenerator.GetDownloadInfoForClientFromInitialDatasetWithParameters(networkCount, nodesPerNetwork, clientsPerNode);

            AssertDoesExistInNetwork(client, initialData);
          }
        }
      }
    }

    [TestMethod]
    public void GeneratesMacAddresForNodeFromInitialDataSetCorrectly()
    {
      for (int networkCount = 1; networkCount < 30; networkCount += 10)
      {
        for (int nodesPerNetwork = 1; nodesPerNetwork < 100; nodesPerNetwork += 200)
        {
          for (int clientsPerNode = 1; clientsPerNode < 100; clientsPerNode += 10)
          {
            var dataGenerator = new Generator.DataGenerator();

            var initialData = dataGenerator.CreateInitialData(networkCount, nodesPerNetwork, clientsPerNode);
            var macAddress = dataGenerator.GetMacAddressForNodeFromInitialDatasetWithParameters(networkCount, nodesPerNetwork);

            AssertDoesExistInNetwork(macAddress, initialData);
          }
        }
      }
    }

    private void AssertDoesExistInNetwork(string macAddress, IEnumerable<Network> initialData)
    {
      var macs = initialData.SelectMany(x => x.Nodes).Select(x => x.MacAddress).ToList();
      bool found = macs.Any(x => x == macAddress);

      Assert.IsTrue(found);
    }

    private void AssertDoesExistInNetwork(ClientDownloadInfo client, IEnumerable<Network> initialData)
    {
      var ips = initialData.SelectMany(x => x.Nodes).SelectMany(x => x.Clients).Select(x => x.IpAddress).ToList();
      bool found = ips.Any(x => x == client.IpAddress);

      Assert.IsTrue(found);
    }

    private static void NetworksAssert(int networkCount, int nodesPerNetwork, int clientsPerNode, IEnumerable<Network> output)
    {
        var newtorks = output.ToList();
        Assert.AreEqual(networkCount, newtorks.Count);
      foreach (var network in newtorks)
      {
        Assert.IsFalse(string.IsNullOrEmpty(network.Location));
        NodesAssert(nodesPerNetwork, clientsPerNode, network);
      }
    }

    private static void NodesAssert(int nodesPerNetwork, int clientsPerNode, Network network)
    {
      Assert.AreEqual(nodesPerNetwork, network.Nodes.Count);

      foreach (var node in network.Nodes)
      {
        ClientsAssert(clientsPerNode, node);
      }
    }

    private static void ClientsAssert(int clientsPerNode, Node node)
    {
      Assert.AreEqual(clientsPerNode, node.Clients.Count);
      foreach (var client in node.Clients)
      {
        Assert.IsFalse(string.IsNullOrEmpty(client.IpAddress));
        Assert.IsTrue(client.BitsDownloaded > 0);
      }
    }
  }
}
