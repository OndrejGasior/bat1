﻿using Microsoft.AspNetCore.Mvc;
using Ninject;
using Batman.Interface.Contract;
using System;
using System.IO;
using System.Reflection;
using Batman.Generator;
using System.Threading.Tasks;
using System.Linq;
using Batman.Business.Interface;
using Batman.Api.IoC;
using Batman.Business.IoC;
using Batman.Persistence.IoC;

namespace Batman.Api.Controllers
{
  /// <summary>
  /// The only API controller
  /// </summary>
  public class BatmanController : Controller
  {
    private static Task _initTask;
    private static string InitialDataGeneratingStatus
    {
      get
      {
        if (_initTask == null)
        {
          return "Not started";
        }
        if (_initTask.IsCompletedSuccessfully)
        {
          return "Ready";
        }
        if (_initTask.IsCompleted)
        {
          return "Something is wrong";
        }
        return "In progress";
      }
    }

    private static readonly IKernel Kernel;
    private static readonly ISettings Settings;

    static BatmanController()
    {
      Kernel = new StandardKernel();
      Kernel.Load(new ApiModule(), new BusinessModule(), new PersistanceModule());
      Settings = Kernel.Get<ISettings>();
    }

    /// <summary>
    /// Uses generated MAC address of a node to get its network.
    /// </summary>
    /// <returns></returns>
    [Route("api/[controller]/random")]
    [HttpGet]
    public async Task<object> Random()
    {
      if (InitialDataGeneratingStatus == "Ready")
      {
        var generator = Kernel.Get<IDataGenerator>();
        var batmanFacade = Kernel.Get<IBatmanFacade>();

        return await batmanFacade.GetNetworkByNode(generator.GetMacAddressForNodeFromInitialDatasetWithParameters(Settings.GetIntValue(Names.Networks), Settings.GetIntValue(Names.NodesPerNetwork)));
      }

      return InitialDataGeneratingStatus;
    }

    /// <summary>
    /// Inserts new network to database.
    /// </summary>
    /// <returns></returns>
    [Route("api/[controller]/insert")]
    [HttpGet]
    public async Task<string> Insert()
    {

      if (InitialDataGeneratingStatus == "Ready")
      {
        var generator = Kernel.Get<IDataGenerator>();
        var batmanFacade = Kernel.Get<IBatmanFacade>();

        var network = generator.CreateRandomNetwork(333, 14);
        await batmanFacade.InsertNetwork(network);

        return "success";
      }
      return InitialDataGeneratingStatus;

    }

    /// <summary>
    /// Adds bits downloaded to client existing in database based on ip address.
    /// </summary>
    /// <returns></returns>
    [Route("api/[controller]/update")]
    [HttpGet]
    public async Task<string> Update()
    {
      if (InitialDataGeneratingStatus == "Ready")
      {
        var generator = Kernel.Get<IDataGenerator>();
        var batmanFacade = Kernel.Get<IBatmanFacade>();

        var info = generator.GetDownloadInfoForClientFromInitialDatasetWithParameters(Settings.GetIntValue(Names.Networks), Settings.GetIntValue(Names.NodesPerNetwork), Settings.GetIntValue(Names.ClientsPerNode));
        await batmanFacade.UpdateClient(info);

        return "success";
      }

      return InitialDataGeneratingStatus;
    }

    /// <summary>
    /// Starts initial data generation.
    /// </summary>
    /// <returns></returns>
    [Route("api/[controller]/startinit")]
    [HttpGet]
    public string StartInit()
    {
      if (_initTask == null)
      {
        _initTask = new Task(GenerateData);
        _initTask.Start();

        return "Starting";
      }

      return InitState();
    }

    /// <summary>
    /// Returns the state of initial data generation.
    /// </summary>
    /// <returns></returns>
    [Route("api/[controller]/initstate")]
    [HttpGet]
    public string InitState()
    {
      return InitialDataGeneratingStatus;
    }

    private void GenerateData()
    {
      var generator = Kernel.Get<IDataGenerator>();
      var batmanFacade = Kernel.Get<IBatmanFacade>();

      batmanFacade.InsertNetworks(generator.CreateInitialData(Settings.GetIntValue(Names.Networks), Settings.GetIntValue(Names.NodesPerNetwork), Settings.GetIntValue(Names.ClientsPerNode)).ToList());
    }
  }
}
