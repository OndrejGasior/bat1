﻿using Batman.Generator;
using Batman.Interface.Contract;
using Ninject.Modules;

namespace Batman.Api.IoC
{
  public class ApiModule : NinjectModule
  {
    public override void Load()
    {
      Bind<IDataGenerator>().To<DataGenerator>();
      Bind<ISettings>().To<HardcodedSettings>();
    }
  }
}
