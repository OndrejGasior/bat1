﻿using Batman.Business.Interface;
using Batman.Interface.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Batman.Business
{
  public class BatmanFacade : IBatmanFacade
  {
    private readonly IWriteService _writeService;
    private readonly IReadService _readService;

    public BatmanFacade(IWriteService writeService, IReadService readService)
    {
      _writeService = writeService;
      _readService = readService;
    }

    public Task<Network> GetNetworkByNode(string macAddress)
    {
      // TODO 11 - Facade implements methods 
      throw new NotImplementedException();
    }

    public Task InsertNetwork(Network network)
    {
      // TODO 11 - Facade implements methods 
      throw new NotImplementedException();
    }

    public Task InsertNetworks(List<Network> networks)
    {
      // TODO 11 - Facade implements methods 
      throw new NotImplementedException();
    }

    public Task UpdateClient(ClientDownloadInfo clientDownloadInfo)
    {
      // TODO 11 - Facade implements methods 
      throw new NotImplementedException();
    }
  }
}
