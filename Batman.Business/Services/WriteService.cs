﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Batman.Business.Interface;
using Batman.Interface.Model;
using Batman.Persistence.Interface;

namespace Batman.Business.Services
{
  /// <summary>
  /// Writes data to storage.
  /// </summary>
  public class WriteService : IWriteService
  {
    private readonly IWriteStorage _writeStorage;

    public WriteService(IWriteStorage writeStorage)
    {
      _writeStorage = writeStorage;
    }

    public Task InsertNetwork(Network network)
    {
      return _writeStorage.InsertNetwork(network);
    }

    public Task InsertNetworks(List<Network> networks)
    {
        return _writeStorage.InsertNetworks(networks);
    }

    public Task UpdateClient(ClientDownloadInfo clientDownloadInfo)
    {
        return _writeStorage.UpdateClient(clientDownloadInfo);
    }
  }
}
