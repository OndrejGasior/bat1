﻿using System;
using System.Text;
using System.Threading.Tasks;
using Batman.Business.Interface;
using Batman.Interface.Contract;
using Batman.Interface.Model;
using Batman.Persistence.Interface;
using Microsoft.Azure.ServiceBus;
using Newtonsoft.Json;

namespace Batman.Business.Services
{
  public class MessageHandler : IMessageHandler
  {
    private IQueueClient _client;
    private readonly ISettings _settings;
    private readonly IWriteStorage _writeStorage;

    public MessageHandler(ISettings settings, IWriteStorage writeStorage)
    {
      _settings = settings;
       _writeStorage = writeStorage;
    }

    public void OnStart()
    {
      _client = new QueueClient(_settings.GetValue(Names.ServiceBusQueueConnectionString), _settings.GetValue(Names.ServiceBusQueueName), ReceiveMode.PeekLock, new RetryExponential(TimeSpan.FromSeconds(0.3), TimeSpan.FromSeconds(2), 5));
    }

    public void Run()
    {
      _client.RegisterMessageHandler(async(receivedMessage, token) =>
      {
        try
        {
          switch(receivedMessage.Label)
          {
            // TODO 13 - Handle messages
            
            default:
              break;
          }
        }
        catch (Exception)
        {
          // What could possibly go wrong ... 
        }
        finally
        {
          await _client.CompleteAsync(receivedMessage.SystemProperties.LockToken);
        }
      }, (exceptionEvent) => Task.CompletedTask
      );
    }

    private T GetObject<T>(byte[] body)
    {
      return JsonConvert.DeserializeObject<T>(Encoding.Unicode.GetString(body));
    }
  }
}