﻿using System.Threading.Tasks;
using Batman.Business.Interface;
using Batman.Interface.Model;
using Batman.Persistence.Interface;

namespace Batman.Business.Services
{
  /// <summary>
  /// Reads data from storage.
  /// </summary>
  public class ReadService : IReadService
  {
    private readonly IReadStorage _readStorage;

    public ReadService(IReadStorage readStorage)
    {
      _readStorage = readStorage;
    }

    public Task<Network> GetNetworkByNode(string macAddress)
    {
      return _readStorage.GetNetworkByNodesMacAddress(macAddress);
    }
  }
}
