﻿using Batman.Business.Interface;
using Batman.Interface.Model;
using System.Collections.Generic;
using System.Threading.Tasks;
using Batman.Persistence.Interface;
using System;

namespace Batman.Business.Services
{
  /// <summary>
  /// Inserts messages to queue.
  /// </summary>
  public class AsyncWriteService : IWriteService
  {
      private readonly IWriteStorage _writeStorage;
      private readonly IMessageQueue _messageQueue;

    public AsyncWriteService(IWriteStorage writeStorage, IMessageQueue messageQueue)
    {
        _writeStorage = writeStorage;
        _messageQueue = messageQueue;
      // TODO
    }

    public Task InsertNetwork(Network network)
    {
      // TODO
     return _writeStorage.InsertNetwork(network);
    }

    public Task InsertNetworks(List<Network> networks)
    {
      return _writeStorage.InsertNetworks(networks);
    }

    public Task UpdateClient(ClientDownloadInfo clientDownloadInfo)
    {
      // TODO 12 - Use message queue
      throw new NotImplementedException();
    }
  }
}
