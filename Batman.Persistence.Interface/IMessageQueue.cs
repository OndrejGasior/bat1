﻿using System.Threading.Tasks;

namespace Batman.Persistence.Interface
{
  public interface IMessageQueue 
  {
      Task SendMessage(string label, object body);
  }
}
