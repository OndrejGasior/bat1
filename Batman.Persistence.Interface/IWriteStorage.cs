﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Batman.Interface.Model;

namespace Batman.Persistence.Interface
{
    public interface IWriteStorage
    {
      Task InsertNetwork(Network network);
      Task InsertNetworks(List<Network> networks);
      Task UpdateClient(ClientDownloadInfo clientDownloadInfo);
    }
}
