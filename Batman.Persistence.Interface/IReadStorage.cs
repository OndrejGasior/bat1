﻿using System.Threading.Tasks;
using Batman.Interface.Model;

namespace Batman.Persistence.Interface
{
  public interface IReadStorage
  {
    Task<Network> GetNetworkByNodesMacAddress(string macAddress);
  }
}
