﻿using System.Collections.Generic;

namespace Batman.Interface.Model
{
  public class Network
  {
    /// <summary>
    /// Unique
    /// </summary>
    public string Location { get; set; }
    public List<Node> Nodes { get; set; }
  }
}