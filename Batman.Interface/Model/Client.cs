﻿namespace Batman.Interface.Model
{
  public class Client
  {
    /// <summary>
    /// Unique
    /// </summary>
    public string IpAddress { get; set; }
    public int BitsDownloaded { get; set; }
  }
}