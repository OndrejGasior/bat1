﻿namespace Batman.Interface.Contract
{
  public interface ISettings
  {
    string GetValue(string key);
    int GetIntValue(string key);
  }
}
