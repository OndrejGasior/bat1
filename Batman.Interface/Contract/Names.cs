﻿namespace Batman.Interface.Contract
{
  /// <summary>
  /// Constants used in the whole solution.
  /// </summary>
  public class Names
  {
    public static readonly string ServiceBusQueueConnectionString = nameof(ServiceBusQueueConnectionString);
    public static readonly string ServiceBusQueueName = nameof(ServiceBusQueueName);
    public static readonly string ClientsPerNode = nameof(ClientsPerNode);
    public static readonly string Networks = nameof(Networks);
    public static readonly string NodesPerNetwork = nameof(NodesPerNetwork);
    public static readonly string StorageConnectionString = nameof(StorageConnectionString);
  }
}
