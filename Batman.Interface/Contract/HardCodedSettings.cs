﻿using System;
using System.Collections.Generic;

namespace Batman.Interface.Contract
{
  public class HardcodedSettings : ISettings
  {
    private readonly Dictionary<string, string> _settings = new Dictionary<string, string>
    {
      [Names.ServiceBusQueueConnectionString] = "",
      [Names.ServiceBusQueueName] = "",
      [Names.Networks] = "10",
      [Names.NodesPerNetwork] = "40",
      [Names.ClientsPerNode] = "50",
      [Names.StorageConnectionString] = "UseDevelopmentStorage=true"
    };

    public string GetValue(string key)
    {
      return _settings[key];
    }

    public int GetIntValue(string key)
    {
      return Int32.Parse(_settings[key]);
    }
  }
}
