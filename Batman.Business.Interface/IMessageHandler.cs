﻿namespace Batman.Business.Interface
{
  public interface IMessageHandler
  {
    void OnStart();
    void Run();
  }
}
