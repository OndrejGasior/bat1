﻿using Batman.Interface.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Batman.Business.Interface
{
  public interface IWriteService
  {
    Task InsertNetwork(Network network);
    Task InsertNetworks(List<Network> networks);
    Task UpdateClient(ClientDownloadInfo clientDownloadInfo);
  }
}
