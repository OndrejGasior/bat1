﻿using System.Threading.Tasks;
using Batman.Interface.Model;

namespace Batman.Business.Interface
{
  public interface IReadService
  {
    Task<Network> GetNetworkByNode(string macAddress);
  }
}