﻿using Batman.Interface.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Batman.Business.Interface
{
    public interface IBatmanFacade
    {
        Task<Network> GetNetworkByNode(string macAddress);

        Task InsertNetworks(List<Network> networks);
        Task InsertNetwork(Network network);
        Task UpdateClient(ClientDownloadInfo clientDownloadInfo);
    }
}
