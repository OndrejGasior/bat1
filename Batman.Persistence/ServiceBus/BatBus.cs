﻿using Batman.Interface.Contract;
using Microsoft.Azure.ServiceBus;
using Newtonsoft.Json;
using System;
using System.Text;
using System.Threading.Tasks;
using Batman.Persistence.Interface;

namespace Batman.Persistence.ServiceBus
{
  public class BatBus : IMessageQueue
  {
    private readonly IQueueClient _client;
    public BatBus(ISettings settings)
    {
      _client = new QueueClient(settings.GetValue(Names.ServiceBusQueueConnectionString), settings.GetValue(Names.ServiceBusQueueName), ReceiveMode.PeekLock, new RetryExponential(TimeSpan.FromSeconds(0.3), TimeSpan.FromSeconds(2), 5));
    }

    public async Task SendMessage(string label, object body)
    {
      var message = new Message() { Label = label, Body = GetBody(body) };
      await _client.SendAsync(message);
    }

    private byte[] GetBody(object message)
    {
      // TODO 14 - Get body
      return null;
    }
  }
}
