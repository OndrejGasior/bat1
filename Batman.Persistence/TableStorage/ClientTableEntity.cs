﻿using Microsoft.WindowsAzure.Storage.Table;

namespace Batman.Persistence.TableStorage
{
  internal class ClientTableEntity : TableEntity
  {
    public int BitsDownloaded { get; set; }

    public ClientTableEntity()
    {

    }

    // TODO 16 - Create client entity
  }
}
