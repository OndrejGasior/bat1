﻿using System.Collections.Generic;
using Batman.Interface.Model;
using Microsoft.WindowsAzure.Storage;
using Batman.Interface.Contract;
using Microsoft.WindowsAzure.Storage.Table;
using System.Linq;
using System.Threading.Tasks;
using Batman.Persistence.Interface;
using System;

namespace Batman.Persistence.TableStorage
{
  public class BatTable : IReadStorage, IWriteStorage
  {
    private readonly CloudTableClient _client;

    public BatTable(ISettings settings)
    {
      var cloudStorageAccount = CloudStorageAccount.Parse(settings.GetValue(Names.StorageConnectionString));
      _client = cloudStorageAccount.CreateCloudTableClient();
    }

    public async Task<Network> GetNetworkByNodesMacAddress(string macAddress)
    {
      CloudTable nodes = _client.GetTableReference(nameof(Node));
      CloudTable clients = _client.GetTableReference(nameof(Client));

      // TODO 21 - Get network

      return new Network { Nodes = null, Location = null };
    }

    public async Task InsertNetwork(Network network)
    {
      CloudTable nodes = _client.GetTableReference(nameof(Node));
      CloudTable clients = _client.GetTableReference(nameof(Client));

      await nodes.CreateIfNotExistsAsync();
      await clients.CreateIfNotExistsAsync();

      TableBatchOperation nodesBatch = new TableBatchOperation();


      // TODO 18 - Insert network

      // Batch works only in the same partition

      

      await nodes.ExecuteBatchAsync(nodesBatch);
    }

    public async Task UpdateClient(ClientDownloadInfo clientDownloadInfo)
    {
      TableQuery<ClientTableEntity> query = null; //... 
      //TODO 20 - Update client

      throw new NotImplementedException();
    }

    public async Task InsertNetworks(List<Network> networks)
    {
      CloudTable nodes = _client.GetTableReference(nameof(Node));
      CloudTable clients = _client.GetTableReference(nameof(Client));

      await nodes.CreateIfNotExistsAsync();
      await clients.CreateIfNotExistsAsync();

      // TODO 19 - Insert networks

      // Batch works only in the same partition
    }
  }
}
