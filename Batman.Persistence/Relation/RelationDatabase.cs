﻿using Batman.Interface.Model;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Batman.Persistence.Interface;
using System;

namespace Batman.Persistence.Relation
{
  public class RelationDatabase : IReadStorage, IWriteStorage
  {
    public static void ClearTables()
    {
      using (var context = new BatmanContext())
      {
        context.Networks.RemoveRange(context.Networks);
        context.SaveChanges();
      }
    }
    public Task<Network> GetNetworkByNodesMacAddress(string macAddress)
    {
      // Use AsQueryable()!!!
      using (var context = new BatmanContext())
      {
        // TODO 8 - Finding network for a node
        throw new NotImplementedException();
      }
    }

    public Task InsertNetwork(Network network)
    {
      using (var context = new BatmanContext())
      {
        throw new NotImplementedException();

        // TODO 7 - Adding network 
      }
      //return Task.CompletedTask;
    }

    private static readonly int BatchSize = 10;
    public Task InsertNetworks(List<Network> networks)
    {
      using (var context = new BatmanContext())
      {
        // TODO 8 - Adding networks 
        throw new NotImplementedException();
      }
      // return Task.CompletedTask;
    }

    public Task UpdateClient(ClientDownloadInfo clientDownloadInfo)
    {
      using (var context = new BatmanContext())
      {
       // TODO 10 - Update client
      }

      return Task.CompletedTask;
    }
  }
}

