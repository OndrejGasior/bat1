﻿using Batman.Interface.Model;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace Batman.Persistence.Relation
{
  public class BatmanContext : DbContext
  {
    public DbSet<Network> Networks { get; set; }
    public List<Node> Nodes { get; set; }
    public List<Client> Clients { get; set; }
    // TODO 3 - Database tables

    public BatmanContext()
    {
      Database.EnsureCreated();
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
      //optionsBuilder.UseSqlServer("connection string");
      optionsBuilder.UseSqlite("Data Source=network.db");
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
      // TODO 4 - Networks table modeling
      // TODO 5 - Nodes table modeling
      // TODO 6 - Client table modeling
    }
  }
}


