﻿using Batman.Interface.Model;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Text;

namespace Batman.Generator
{
  public class DataGenerator : IDataGenerator
  {
    private static readonly Random Random = new Random();

    public string GetMacAddressForNodeFromInitialDatasetWithParameters(int networkCount, int nodesPerNetwork)
    {
      return GetInitialDataMacAddress(networkCount, nodesPerNetwork);
    }

    public ClientDownloadInfo GetDownloadInfoForClientFromInitialDatasetWithParameters(int networkCount, int nodesPerNetwork, int clientsPerNode)
    {
      return new ClientDownloadInfo() { IpAddress = GetInitialDataIpAddress(networkCount, nodesPerNetwork, clientsPerNode), BitsDownloaded = GetRandomBiteNumber() };
    }

    public IEnumerable<Network> CreateInitialData(int networkCount, int nodesPerNetwork, int clientsPerNode)
    {
      return Enumerable.Range(1, networkCount).AsParallel().Select(x => CreateNetwork(x, nodesPerNetwork, clientsPerNode, true));
    }

    public Network CreateRandomNetwork(int nodesPerNetwork, int clientsPerNode)
    {
      return CreateNetwork(1, nodesPerNetwork, clientsPerNode, false);
    }

    private Network CreateNetwork(int networkOrderNumber, int nodesPerNetwork, int clientsPerNode, bool initialData)
    {
      return new Network()
      {
        Nodes = Enumerable.Range(1, nodesPerNetwork).Select(x => CreateNode(networkOrderNumber, x, clientsPerNode, initialData)).ToList(),
        Location = GetLocation(networkOrderNumber, initialData)
      };
    }

    private Node CreateNode(int networkOrderNumber, int nodeOrderNumber, int clientsPerNode, bool initialData)
    {
      return new Node()
      {
        Clients = Enumerable.Range(1, clientsPerNode).Select(x => CreateClient(networkOrderNumber, nodeOrderNumber, x, initialData)).ToList(),
        MacAddress = GetMacAddress(networkOrderNumber, nodeOrderNumber, initialData)
      };
    }

    private Client CreateClient(int networkOrderNumber, int nodeOrderNumber, int clientOrderNumber, bool initialData)
    {
      return new Client() { BitsDownloaded = GetRandomBiteNumber(), IpAddress = GetIpAddress(networkOrderNumber, nodeOrderNumber, clientOrderNumber, initialData) };
    }

    private string GetIpAddress(int networkOrderNumber, int nodeOrderNumber, int clientOrderNumber, bool initialData)
    {
      return initialData ? GetIpAddressForInitialData(networkOrderNumber, nodeOrderNumber, clientOrderNumber) : GetRandomIpAddress();
    }

    private string GetLocation(int networkOrder, bool initialData)
    {
      return initialData ? "location: " + networkOrder : Guid.NewGuid().ToString();
    }

    private string GetMacAddress(int networkOrderNumber, int nodeOrderNumber, bool initialData)
    {
      return initialData ? GetMacAddressForInitialData(networkOrderNumber, nodeOrderNumber) : GetRandomMacAddress();
    }

    private int GetRandomBiteNumber()
    {
      return Random.Next(1, 1000);
    }

    public static string ToStringGuid(byte[] value)
    {
      return new Guid(value).ToString();
    }

    private string GetRandomMacAddress()
    {
      return Guid.NewGuid() + Guid.NewGuid().ToString();
    }

    private string GetRandomIpAddress()
    {
      return Guid.NewGuid() + Guid.NewGuid().ToString();
    }

    private string GetIpAddressForInitialData(int networkOrderNumber, int nodeOrderNumber, int clientOrderNumber)
    {
      StringBuilder ipAddress = new StringBuilder();

      ipAddress.Append(Convert.ToString(networkOrderNumber, 16));
      ipAddress.Append(".");
      ipAddress.Append(Convert.ToString(nodeOrderNumber, 16));
      ipAddress.Append(".");
      ipAddress.Append(Convert.ToString(clientOrderNumber, 16));

      return ipAddress.ToString();
    }

    private string GetMacAddressForInitialData(int networkOrderNumber, int nodeOrderNumber)
    {
      StringBuilder macAddress = new StringBuilder();

      macAddress.Append(Convert.ToString(networkOrderNumber, 16));
      macAddress.Append(":");
      macAddress.Append(Convert.ToString(nodeOrderNumber, 16));

      return macAddress.ToString();
    }

    private string GetInitialDataIpAddress(int networkCount, int nodesPerNetwork, int clientsPerNode)
    {
      return GetIpAddressForInitialData(Random.Next(1, networkCount), Random.Next(1, nodesPerNetwork), Random.Next(1, clientsPerNode));
    }

    private string GetInitialDataMacAddress(int networkCount, int nodesPerNetwork)
    {
      return GetMacAddressForInitialData(Random.Next(1, networkCount), Random.Next(1, nodesPerNetwork));
    }
  }
}
