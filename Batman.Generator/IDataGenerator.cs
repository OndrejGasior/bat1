﻿using Batman.Interface.Model;
using System.Collections.Generic;

namespace Batman.Generator
{
  public interface IDataGenerator
  {
    string GetMacAddressForNodeFromInitialDatasetWithParameters(int networkCount, int nodesPerNetwork);
    ClientDownloadInfo GetDownloadInfoForClientFromInitialDatasetWithParameters(int networkCount, int nodesPerNetwork, int clientsPerNode);
    IEnumerable<Network> CreateInitialData(int networkCount, int nodesPerNetwork, int clientsPerNode);
    Network CreateRandomNetwork(int nodesPerNetwork, int clientsPerNode);
  }
}