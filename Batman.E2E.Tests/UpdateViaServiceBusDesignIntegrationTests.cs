﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Batman.Business;
using Batman.Business.Interface;
using Batman.Business.Services;
using Batman.Generator;
using Batman.Interface.Contract;
using Batman.Interface.Model;
using Batman.Persistence.Interface;
using Batman.Persistence.Relation;
using Batman.Persistence.ServiceBus;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ninject;
using Ninject.Modules;

namespace Batman.E2E.Tests
{
  [TestClass]
    public class UpdateViaServiceBusDesignIntegrationTests
    {
        private class Settings : ISettings
        {
            private readonly Dictionary<string, string> _settings = new Dictionary<string, string>
            {
                [Names.ServiceBusQueueConnectionString] = "Endpoint=sb://npmtests.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=tsTfaTKunqfyTsg3wEry+KNLAtofxmuIRGLY+9Y2NPc=",
                [Names.ServiceBusQueueName] = "npmtestservicebus",
                [Names.Networks] = "10",
                [Names.NodesPerNetwork] = "10",
                [Names.ClientsPerNode] = "5",
                [Names.StorageConnectionString] = "UseDevelopmentStorage=true"
            };

            public string GetValue(string key)
            {
                return _settings[key];
            }

            public int GetIntValue(string key)
            {
                return int.Parse(_settings[key]);
            }
        }

        private class Module : NinjectModule
        {
            public override void Load()
            {
                Bind<ISettings>().To<Settings>();
                Bind<IBatmanFacade>().To<BatmanFacade>();
                Bind<IWriteService>().To<AsyncWriteService>();
                Bind<IReadService>().To<ReadService>();
                Bind<IDataGenerator>().To<DataGenerator>();
                Bind<IMessageHandler>().To<MessageHandler>();
            }
        }
        private readonly IKernel _kernel;

        public UpdateViaServiceBusDesignIntegrationTests()
        {
            _kernel = new StandardKernel(new Module());
        }

        [TestMethod]
        public async Task UpdatesClientCorrectly()
        {
            RelationDatabase.ClearTables();
            _kernel.Bind<IWriteStorage>().To<RelationDatabase>();
            _kernel.Bind<IReadStorage>().To<RelationDatabase>();
            _kernel.Bind<IMessageQueue>().To<BatBus>();

            var facade = _kernel.Get<IBatmanFacade>();
            var generator = _kernel.Get<IDataGenerator>();
            var settings = _kernel.Get<ISettings>();
            var messageHandler = _kernel.Get<IMessageHandler>();
            messageHandler.OnStart();
            messageHandler.Run();

            var output = generator.CreateInitialData(settings.GetIntValue(Names.Networks), settings.GetIntValue(Names.NodesPerNetwork), settings.GetIntValue(Names.ClientsPerNode)).ToList();
            await facade.InsertNetworks(output);

            foreach (var ipAddress in output.SelectMany(x => x.Nodes).SelectMany(x => x.Clients).Select(x => x.IpAddress).Take(3))
            {
                int before;
                int after;
                using (var context = new BatmanContext())
                {
                    before = context.Clients.AsQueryable().Single(x => x.IpAddress == ipAddress).BitsDownloaded;
                }
                await facade.UpdateClient(new ClientDownloadInfo { IpAddress = ipAddress, BitsDownloaded = 100 });
                await Task.Delay(1000);
                using (var context = new BatmanContext())
                {
                    after = context.Clients.AsQueryable().Single(x => x.IpAddress == ipAddress).BitsDownloaded;
                }

                Assert.AreEqual(before + 100, after);
            }
        }
    }
}
