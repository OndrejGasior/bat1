﻿using Batman.Business;
using Batman.Business.Interface;
using Batman.Business.Services;
using Batman.Generator;
using Batman.Interface.Contract;
using Batman.Interface.Model;
using Batman.Persistence.Interface;
using Batman.Persistence.TableStorage;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ninject;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Batman.E2E.Tests
{
  [TestClass]
  public class TableStorageDesignIntegrationTests
  {
    private class Settings : ISettings
    {
      private readonly Dictionary<string, string> _settings = new Dictionary<string, string>
      {
        [Names.ServiceBusQueueConnectionString] = "",
        [Names.ServiceBusQueueName] = "",
        [Names.Networks] = "10",
        [Names.NodesPerNetwork] = "10",
        [Names.ClientsPerNode] = "5",
        [Names.StorageConnectionString] = "DefaultEndpointsProtocol=https;AccountName=npmtests;AccountKey=LPwoxHDOCzvRbpHnLFL3tPLfsSTNsAYRkyWd1txYbgVues1S68LChMXDcv7U2ShisI88iOsIiwzxhkiJK181yA==;EndpointSuffix=core.windows.net"
      };

      public string GetValue(string key)
      {
        return _settings[key];
      }

      public int GetIntValue(string key)
      {
        return Int32.Parse(_settings[key]);
      }
    }

    private class Module : NinjectModule
    {
      public override void Load()
      {
        Bind<ISettings>().To<Settings>();
        Bind<IBatmanFacade>().To<BatmanFacade>();
        Bind<IWriteService>().To<WriteService>();
        Bind<IReadService>().To<ReadService>();
        Bind<IDataGenerator>().To<DataGenerator>();
      }
    }

    private readonly IKernel _kernel;

    public TableStorageDesignIntegrationTests()
    {
      _kernel = new StandardKernel(new Module());
    }

    [TestMethod]
    public async Task SavesNetworkCorrectly()
    {
      _kernel.Bind<IWriteStorage>().To<BatTable>();
      _kernel.Bind<IReadStorage>().To<BatTable>();

      var facade = _kernel.Get<IBatmanFacade>();
      var generator = _kernel.Get<IDataGenerator>();
      var settings = _kernel.Get<ISettings>();

      var output = generator.CreateInitialData(settings.GetIntValue(Names.Networks), settings.GetIntValue(Names.NodesPerNetwork), settings.GetIntValue(Names.ClientsPerNode)).ToList();

      foreach (var network in output.Take(2))
      {
        await facade.InsertNetwork(network);
        Assert.AreEqual(network.Location, (await facade.GetNetworkByNode(network.Nodes.First().MacAddress)).Location);
      }
    }

    [TestMethod]
    public async Task SavesNetworksCorrectly()
    {
      _kernel.Bind<IWriteStorage>().To<BatTable>();
      _kernel.Bind<IReadStorage>().To<BatTable>();

      var facade = _kernel.Get<IBatmanFacade>();
      var generator = _kernel.Get<IDataGenerator>();
      var settings = _kernel.Get<ISettings>();

      var output = generator.CreateInitialData(settings.GetIntValue(Names.Networks), settings.GetIntValue(Names.NodesPerNetwork), settings.GetIntValue(Names.ClientsPerNode)).ToList();

      await facade.InsertNetworks(output);
      
      foreach (var network in output)
      {
        Assert.AreEqual(network.Location, (await facade.GetNetworkByNode(network.Nodes.First().MacAddress)).Location);
      }
    }

    [TestMethod]
    public async Task LoadsNetworkCorrectly()
    {
      _kernel.Bind<IWriteStorage>().To<BatTable>();
      _kernel.Bind<IReadStorage>().To<BatTable>();
      
      var settings = _kernel.Get<ISettings>();
      var generator = _kernel.Get<IDataGenerator>();
      var facade = _kernel.Get<IBatmanFacade>();

      var output = generator.CreateInitialData(settings.GetIntValue(Names.Networks), settings.GetIntValue(Names.NodesPerNetwork), settings.GetIntValue(Names.ClientsPerNode)).ToList();
      await facade.InsertNetworks(output);

      var firstNetwork = output.First();
      var firstNodeOfFirstNetwork = firstNetwork.Nodes.First();

      var result = await facade.GetNetworkByNode(firstNodeOfFirstNetwork.MacAddress);

      Assert.AreEqual(firstNetwork.Location, result.Location);
    }

    [TestMethod]
    public async Task UpdatesClientCorrectly()
    {
      _kernel.Bind<IWriteStorage>().To<BatTable>();
      _kernel.Bind<IReadStorage>().To<BatTable>();

      var facade = _kernel.Get<IBatmanFacade>();
      var generator = _kernel.Get<IDataGenerator>();
      var settings = _kernel.Get<ISettings>();

      var output = generator.CreateInitialData(settings.GetIntValue(Names.Networks), settings.GetIntValue(Names.NodesPerNetwork), settings.GetIntValue(Names.ClientsPerNode)).ToList();
      await facade.InsertNetworks(output);

      foreach (var node in output.SelectMany(x => x.Nodes).Take(3))
      {
        var client = node.Clients.First();
        int before = client.BitsDownloaded;

        await facade.UpdateClient(new ClientDownloadInfo() { IpAddress = client.IpAddress, BitsDownloaded = 100 });
        var nodes = (await facade.GetNetworkByNode(node.MacAddress)).Nodes;
        var clients = nodes.Single(x => x.MacAddress == node.MacAddress).Clients;
        var after = clients.Single(x => x.IpAddress == client.IpAddress).BitsDownloaded;

        Assert.AreEqual(before + 100, after);
      }
    }
  }
}