﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Batman.Business;
using Batman.Business.Interface;
using Batman.Business.Services;
using Batman.Generator;
using Batman.Interface.Contract;
using Batman.Interface.Model;
using Batman.Persistence.Interface;
using Batman.Persistence.Relation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ninject;
using Ninject.Modules;

namespace Batman.E2E.Tests
{
    [TestClass]
    public class RelationDatabaseDesignIntegrationTests
    {
        private class Settings : ISettings
        {
            private readonly Dictionary<string, string> _settings = new Dictionary<string, string>
            {
                [Names.ServiceBusQueueConnectionString] = "",
                [Names.ServiceBusQueueName] = "",
                [Names.Networks] = "10",
                [Names.NodesPerNetwork] = "10",
                [Names.ClientsPerNode] = "5"
            };

            public string GetValue(string key)
            {
                return _settings[key];
            }

            public int GetIntValue(string key)
            {
                return Int32.Parse(_settings[key]);
            }
        }

        private class Module : NinjectModule
        {
            public override void Load()
            {
                Bind<ISettings>().To<Settings>();
                Bind<IBatmanFacade>().To<BatmanFacade>();
                Bind<IWriteService>().To<WriteService>();
                Bind<IReadService>().To<ReadService>();
                Bind<IDataGenerator>().To<DataGenerator>();
            }
        }
        private readonly IKernel _kernel;

        public RelationDatabaseDesignIntegrationTests()
        {
            _kernel = new StandardKernel(new Module());
        }

        [TestMethod]
        public async Task SaveCallsCorrectService()
        {
            var writeStorage = new WriteStorageSpy();

            _kernel.Bind<IWriteStorage>().ToConstant(writeStorage);
            var readStorage = new ReadStorageSpy(null);
            _kernel.Bind<IReadStorage>().ToConstant(readStorage);

            var facade = _kernel.Get<IBatmanFacade>();
            var generator = _kernel.Get<IDataGenerator>();
            var settings = _kernel.Get<ISettings>();

            var output = generator.CreateInitialData(settings.GetIntValue(Names.Networks), settings.GetIntValue(Names.NodesPerNetwork), settings.GetIntValue(Names.ClientsPerNode)).ToList();

            foreach (var network in output)
            {
                await facade.InsertNetwork(network);
            }

            Assert.AreEqual(output.Count, writeStorage.InsertWasCalledTimes);
        }

        [TestMethod]
        public async Task SavesNetworkCorrectly()
        {
            RelationDatabase.ClearTables();
            _kernel.Bind<IWriteStorage>().To<RelationDatabase>();
            _kernel.Bind<IReadStorage>().To<RelationDatabase>();

            var facade = _kernel.Get<IBatmanFacade>();
            var generator = _kernel.Get<IDataGenerator>();
            var settings = _kernel.Get<ISettings>();

            var output = generator.CreateInitialData(settings.GetIntValue(Names.Networks), settings.GetIntValue(Names.NodesPerNetwork), settings.GetIntValue(Names.ClientsPerNode)).ToList();

            foreach (var network in output)
            {
               await facade.InsertNetwork(network);
            }

            using (BatmanContext context = new BatmanContext())
            {
                Assert.AreEqual(output.Count(), context.Networks.Count());
                Assert.AreEqual(output.SelectMany(x => x.Nodes).Count(), context.Nodes.Count());
                Assert.AreEqual(output.SelectMany(x => x.Nodes).SelectMany(x => x.Clients).Count(), context.Clients.Count());
            }
        }


        [TestMethod]
        public async Task SavesNetworksCorrectly()
        {
            RelationDatabase.ClearTables();
            _kernel.Bind<IWriteStorage>().To<RelationDatabase>();
            _kernel.Bind<IReadStorage>().To<RelationDatabase>();

            var facade = _kernel.Get<IBatmanFacade>();
            var generator = _kernel.Get<IDataGenerator>();
            var settings = _kernel.Get<ISettings>();

            var output = generator.CreateInitialData(settings.GetIntValue(Names.Networks), settings.GetIntValue(Names.NodesPerNetwork), settings.GetIntValue(Names.ClientsPerNode)).ToList();

            await facade.InsertNetworks(output);

            using (BatmanContext context = new BatmanContext())
            {
                Assert.AreEqual(output.Count(), context.Networks.Count());
                Assert.AreEqual(output.SelectMany(x => x.Nodes).Count(), context.Nodes.Count());
                Assert.AreEqual(output.SelectMany(x => x.Nodes).SelectMany(x => x.Clients).Count(), context.Clients.Count());
            }
        }

        [TestMethod]
        public async Task CallsLoadsNetworkCorrectly()
        {
            var writeStorage = new WriteStorageSpy();
            _kernel.Bind<IWriteStorage>().ToConstant(writeStorage);
            var settings = _kernel.Get<ISettings>();
            var generator = _kernel.Get<IDataGenerator>();

            var output = generator.CreateInitialData(settings.GetIntValue(Names.Networks), settings.GetIntValue(Names.NodesPerNetwork), settings.GetIntValue(Names.ClientsPerNode)).ToList().First();
            var readStorage = new ReadStorageSpy(output);
            _kernel.Bind<IReadStorage>().ToConstant(readStorage);
            var facade = _kernel.Get<IBatmanFacade>();

            var result = await facade.GetNetworkByNode("");

            Assert.AreEqual(output.Location, result.Location);
        }

        [TestMethod]
        public async Task LoadsNetworkCorrectly()
        {
            RelationDatabase.ClearTables();
            _kernel.Bind<IWriteStorage>().To<RelationDatabase>();
            _kernel.Bind<IReadStorage>().To<RelationDatabase>();

            var settings = _kernel.Get<ISettings>();
            var generator = _kernel.Get<IDataGenerator>();

            var output = generator.CreateInitialData(settings.GetIntValue(Names.Networks), settings.GetIntValue(Names.NodesPerNetwork), settings.GetIntValue(Names.ClientsPerNode)).ToList();
            var facade = _kernel.Get<IBatmanFacade>();
            await facade.InsertNetworks(output);

            var result = await facade.GetNetworkByNode(output.First().Nodes.First().MacAddress);

            Assert.AreEqual(output.First().Location, result.Location);
        }

        [TestMethod]
        public async Task CallsUpdateClientCorrectly()
        {
            var writeStorage = new WriteStorageSpy();

            _kernel.Bind<IWriteStorage>().ToConstant(writeStorage);
            var readStorage = new ReadStorageSpy(null);
            _kernel.Bind<IReadStorage>().ToConstant(readStorage);

            var facade = _kernel.Get<IBatmanFacade>();
            var generator = _kernel.Get<IDataGenerator>();
            var settings = _kernel.Get<ISettings>();

            var output = generator.CreateInitialData(settings.GetIntValue(Names.Networks), settings.GetIntValue(Names.NodesPerNetwork), settings.GetIntValue(Names.ClientsPerNode)).ToList();

            foreach (var ipAddress in output.SelectMany(x => x.Nodes).SelectMany(x => x.Clients).Select(x => x.IpAddress))
            {
                await facade.UpdateClient(new ClientDownloadInfo() { IpAddress = ipAddress, BitsDownloaded = 100 });
            }

            Assert.AreEqual(output.SelectMany(x => x.Nodes).SelectMany(x => x.Clients).Count(), writeStorage.UpdateWasCalledTimes);
        }

        [TestMethod]
        public async Task UpdatesClientCorrectly()
        {
            RelationDatabase.ClearTables();
            _kernel.Bind<IWriteStorage>().To<RelationDatabase>();
            _kernel.Bind<IReadStorage>().To<RelationDatabase>();

            var facade = _kernel.Get<IBatmanFacade>();
            var generator = _kernel.Get<IDataGenerator>();
            var settings = _kernel.Get<ISettings>();

            var output = generator.CreateInitialData(settings.GetIntValue(Names.Networks), settings.GetIntValue(Names.NodesPerNetwork), settings.GetIntValue(Names.ClientsPerNode)).ToList();
            await facade.InsertNetworks(output);

            foreach (var ipAddress in output.SelectMany(x => x.Nodes).SelectMany(x => x.Clients).Select(x => x.IpAddress).Take(5))
            {
                int before;
                int after;
                using (var context = new BatmanContext())
                {
                    before = context.Clients.AsQueryable().Single(x => x.IpAddress == ipAddress).BitsDownloaded;
                }
                await facade.UpdateClient(new ClientDownloadInfo() { IpAddress = ipAddress, BitsDownloaded = 100 });
                using (var context = new BatmanContext())
                {
                    after = context.Clients.AsQueryable().Single(x => x.IpAddress == ipAddress).BitsDownloaded;
                }
                Assert.AreEqual(before + 100, after);
            }
        }

        private class WriteStorageSpy : IWriteStorage
        {
            public int InsertWasCalledTimes { get; private set; }
            public int UpdateWasCalledTimes { get; private set; }

            public Task InsertNetwork(Network network)
            {
                InsertWasCalledTimes++;
                return Task.CompletedTask;
            }

            public Task InsertNetworks(List<Network> networks)
            {
                throw new NotImplementedException();
            }

            public Task UpdateClient(ClientDownloadInfo clientDownloadInfo)
            {
                UpdateWasCalledTimes++;
                return Task.CompletedTask;
            }
        }

        private class ReadStorageSpy : IReadStorage
        {
            private readonly Network _network;

            public ReadStorageSpy(Network network)
            {
                _network = network;
            }

            public Task<Network> GetNetworkByNodesMacAddress(string macAddress)
            {
                return Task.FromResult(_network);
            }
        }
    }
}