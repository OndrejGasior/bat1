﻿using Batman.Business.Interface;
using Batman.Business.Services;
using Batman.Interface.Contract;
using Ninject.Modules;

namespace Batman.WebJob.IoC
{
    public class WebJobModule : NinjectModule
    {
        public override void Load()
        {
            Bind<ISettings>().To<HardcodedSettings>();
            Bind<IMessageHandler>().To<MessageHandler>();
        }
    }
}
