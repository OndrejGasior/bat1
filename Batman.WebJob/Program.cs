﻿using System;
using System.IO;
using Ninject;
using System.Reflection;
using Batman.Business.Interface;
using Batman.WebJob.IoC;
using Batman.Business.IoC;
using Batman.Persistence.IoC;

namespace Batman.WebJob
{
  public class Program
  {
    private static readonly IKernel Kernel;

    static Program()
    {
      Kernel = new StandardKernel();
      Kernel.Load(new WebJobModule(), new BusinessModule(), new PersistanceModule());
    }

    public static void Main(string[] args)
    {
      Console.WriteLine("Hello NPM!");
      var messageHandler = Kernel.Get<IMessageHandler>();
      messageHandler.OnStart();
      messageHandler.Run();
      while (true)
      {

      }
    }
  }
}